const Pizza = require('./pizza');

module.exports = () => {
    const result = [];
    result.push(new Pizza("Pepperoni",12.99));
    result.push(new Pizza("Cheese and Tomato",10.99));
    return result;
}