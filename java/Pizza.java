package com.pizzaordering;

public class Pizza implements IBasketItem
{
    private String _name="";
    private float _price=0;  

    public Pizza(String name,float price){
        _name = name;
        _price = price;
    }

    public String getName(){
        return _name;
    }

    public float getPrice(){
        return _price;
    }

}