package com.pizzaordering;

public class Menu{
    public Pizza[] Pizzas(){
        Pizza[] result = new Pizza[2];
     
        result[0]=new Pizza("Pepperoni",12.99F);
        result[1]=new Pizza("Cheese and Tomato",10.99F);
        
        return result;   
    }
}