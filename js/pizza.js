class Pizza{
    constructor(name,price){
        this.name = name;
        this.price = price
    }

    name(){
        return this.name;
    }

    price(){
        return this.price;
    }
}

module.exports = Pizza;