package com.pizzaordering;

import java.util.ArrayList;
import java.util.List;

public class Basket {

    private List<IBasketItem> _items;

    public Basket(){
        _items = new ArrayList<IBasketItem>();
    }

    public int itemCount(){
        return _items.size();
    }

    public List<IBasketItem> getItems(){
        return _items;
    }

    public float total(){
        float result=0;

        for(IBasketItem item:_items){
            result+=item.getPrice();
        }
        return result;
    }
    public void add(IBasketItem item){
        _items.add(item);
    }
}