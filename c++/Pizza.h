#include <string>

class Pizza{
    private:        
        float _price;
        std::string _name;
    public:
        Pizza(std::string name,float price);
        std::string getName();
        float getPrice();        
};