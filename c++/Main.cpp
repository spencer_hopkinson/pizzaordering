#include <iostream>
#include "Basket.h"
#include "Pizza.h"
#include "Menu.cpp"

using namespace std;

Basket basket;

void PrintBasket(){
    cout << "Item count:" << basket.ItemCount() << endl;
    cout << "" << endl;
    cout << "Basket total:" << basket.Total() << endl;
}

int main(){
    cout<<"Hello Spenny Wenny!!!";

    PrintBasket();

    Pizza pizza("test",10.00);

    cout << "Pizza name: " << pizza.getName() << " Pizza Price: " << pizza.getPrice() << endl;

    return 0;
}