package com.pizzaordering;

public interface IBasketItem{
    float getPrice();
    String getName();    
}