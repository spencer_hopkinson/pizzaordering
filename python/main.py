from menu import get_menu
from basket import Basket

def print_basket():
    print()
    print("Number of items in basket: %s" % (basket.itemCount))
    print()
    print("Basket total: %2f" % (basket.total))
    print()
    print('--Basket Contents:--')
    for item in basket.contents:
        print(item.name)

print("Please select a pizza to add to your basket:")
print()

pizzas = get_menu()
basket = Basket()

x=1
for pizza in pizzas:
    print("%d Pizza: %s Price: %2f" % (x,pizza.name,pizza.price))
    x+=1

print_basket()

basket.add(pizzas[0])
print_basket()

basket.add(pizzas[1])
print_basket()

basket.add(pizzas[1])
print_basket()