﻿using System.Collections.Generic;

namespace app
{
    class Basket
    {
        private List<IBasketItem> _items;

        public Basket()
        {
            _items = new List<IBasketItem>();
        }

        public int ItemCount { get => _items.Count; }

        public IList<IBasketItem> Items { get => _items; }

        public decimal Total
        {
            get
            {
                decimal result = 0;

                foreach(IBasketItem item in _items)
                {
                    result += item.Price;
                }

                return result;
            }            
        }

        public void Add(IBasketItem item)
        {
            _items.Add(item);
        }

    }
}
