class Basket{
    constructor(){
        this.items=[];
    }   

    add(item) {
        this.items.push(item);
    }
    remove(item){

    }

    total(){
        let total = 0;
        this.items.forEach(x=>{
            total+= x.price;
        })
        
        return total;
    }

    itemCount(){
        return this.items.length;
    }

    contents(){
        return this.items;
    }
}

module.exports = Basket