class Basket:

    def __init__(self):
        self.__items = []

    def add(self,item):
        self.__items.append(item)

    def remove(self,item):
        pass

    @property
    def total(self):
        temp = 0
        for item in self.__items:
            temp += item.price

        return temp

    @property
    def itemCount(self):
        return len(self.__items)

    @property
    def contents(self):
        return self.__items
