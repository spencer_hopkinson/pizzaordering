param($makeJar=$false)
Clear-Host

if((Test-Path "bin"))
{
    Remove-Item "bin" -Force -Recurse
}

Write-Host "Compiling..." -ForegroundColor Yellow

javac *.java -d bin

Set-Location ./bin

if($makeJar)
{
    Write-Host "Making jar..." -ForegroundColor Yellow
    #output jar file / entry point / source files for jar
    jar cfe pizzaordering.jar com.pizzaordering.Main com\pizzaordering\*.class

    Write-Host "Running jar..." -ForegroundColor Yellow
    java -jar pizzaordering.jar
}
else
{
    Write-Host "Running app..." -ForegroundColor Yellow
    java com.pizzaordering.Main
}

Set-Location ..