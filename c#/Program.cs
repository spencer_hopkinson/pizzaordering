﻿using System;

namespace app
{
    class Program
    {
        private static Basket _basket = new Basket();

        static void Main(string[] args)
        {
            Menu menu = new Menu();
            int count = 1;

            foreach(Pizza pizza in menu.Pizzas)
            {
                Console.WriteLine($"{count} Pizza: {pizza.Name} Price: {pizza.Price}");                
            }

            PrintBasket();

            _basket.Add(menu.Pizzas[0]);
            PrintBasket();

            _basket.Add(menu.Pizzas[1]);
            PrintBasket();

            _basket.Add(menu.Pizzas[1]);
            PrintBasket();

            Console.ReadLine();
        }

        static void PrintBasket()
        {
            Console.WriteLine();
            Console.WriteLine($"Number of items in basket: {_basket.ItemCount}");
            Console.WriteLine("");
            Console.WriteLine($"Basket total: {_basket.Total}");
            Console.WriteLine();
            Console.WriteLine("--Basket Contents:--");

            foreach(IBasketItem item in _basket.Items)
            {
                Console.WriteLine(item.Name);
            }
        }
    }
}
