﻿namespace app
{
    class Pizza : IBasketItem
    {
        public Pizza(string name,decimal price)
        {
            Name = name;
            Price = price;
        }

        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
