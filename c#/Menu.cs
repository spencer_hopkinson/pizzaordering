﻿using System.Collections.Generic;

namespace app
{
    class Menu
    {
        public IList<Pizza> Pizzas
        {
            get
            {
                List<Pizza> result = new List<Pizza>();

                result.Add(new Pizza("Pepperoni", 12.99M));
                result.Add(new Pizza("Cheese and Tomato", 10.99M));

                return result;
            }
        }
    }
}
