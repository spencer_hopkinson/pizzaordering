package com.pizzaordering;

public class Main
{
    private static Basket _basket = new Basket();
    public static void main(String args[])
    {
        System.out.println("Please select a pizza to add to your basket:");
        System.out.println();

        Menu menu = new Menu();

        int count=1;

        for(Pizza pizza:menu.Pizzas()){
            System.out.printf("%s Pizza: %s Price %.2f",count,pizza.getName(),pizza.getPrice());
            System.out.println();
            count++;
        }

        PrintBasket();

        _basket.add(menu.Pizzas()[0]);        
        PrintBasket();

        _basket.add(menu.Pizzas()[1]);
        PrintBasket();

        _basket.add(menu.Pizzas()[1]);
        PrintBasket();
    }

    private static void PrintBasket()
    {                
        System.out.println();
        System.out.printf("Number of items in basket: %s",_basket.itemCount());
        System.out.println("");
        System.out.printf("Basket total: %.2f",_basket.total());
        System.out.println();
        System.out.println("--Basket Contents:--");
        
        for(IBasketItem item:_basket.getItems()){
            System.out.println(item.getName());
        }        
    }
}