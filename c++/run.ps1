Clear-Host

if((Test-Path "bin") -eq  $false)
{
    New-Item bin -ItemType Directory
}

$runWindows = $true

if($IsWindows -eq $null){
	$runWindows = $true
}

if($IsWindows -or $runWindows)
{
	g++ *.cpp -o ./bin/pizza.exe
	Set-Location ./bin
	.\pizza.exe
}

if($IsLinux -eq $true)
{
	g++ *.cpp -o ./bin/pizza
	Set-Location ./bin
	./pizza
}

Set-Location ..