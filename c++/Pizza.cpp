#include "Pizza.h"

using namespace std;

Pizza::Pizza(string name,float price){
    _name = name;
    _price = price;
}

string Pizza::getName(){
    return _name;
}

float Pizza::getPrice(){
    return _price;
}