﻿namespace app
{
    interface IBasketItem
    {
        string Name { get; set; }
        decimal Price { get; set; }
    }
}
